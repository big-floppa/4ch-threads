# 4ch Threads

### Downloads all images from a 4chan thread
#### Usage:
python3 script.py -t https://4chan.org/<board>/thread/23234 -b wg

Saves images to ~/Downloads/4ch/<board>/
