import os
import re
import sys
import getopt
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm


class ImageData:
    url:   str = None
    name:  str = None
    path:  str #= None
    board: str = None
    def __init__(self, url_: str, name_: str, board_: str=None, thread_: str=None):
        self.url = url_
        self.name = name_
        self.board = board_
        self.path = None
        self.thread = thread_

def save(path: str, image: ImageData):
    response = requests.get(image.url).content
    with open(f'{path}/{image.name}', 'wb') as file:
        file.write(response)
    return True

def get_images_list( soup, board, thread):
    tags = [x.get('href') for x in soup.findAll('a',class_='fileThumb')]
    images = [
        ImageData(
            x.replace('//','https://'),
            re.search(r'\/[\d]{1,16}\.(jpg)?(png)?(webm)?(gif)?', x).group(0).replace('/', ''),
            board_=board,
            thread_=thread
        ) for x in tags
    ]

    return images

if __name__ == "__main__":
    Usage = 'Usage: -t <thread> -b <board abbrv>'
    argv = sys.argv[1:]
    board = None
    thread = None
    try:
        opts, args = getopt.getopt(argv, 't:b:')
    except getopt.GetoptError:
        print(Usage)
        exit(0)
    if len(opts) == 0:
        print(Usage)
        exit(0)

    for opt, arg in opts:
        if opt in ['-t']:
            thread = arg
        if opt in ['-b']:
            board = arg
    if thread is None:
        print(Usage)
        exit(0)
    try:
        content = requests.get(thread).content
        soup = BeautifulSoup(content, 'lxml')
    except:
        print("Enter valid thread url", Usage, '\n')
    if board is None:
        try:
            board_ = re.search(r'org\/[\w]{1,4}/thread', thread).group(0)
            board = board_.replace('org/', '').replace('/thread', '')
        except:
            print('Cannot find board name, specify with -b ')
            exit(0)
    
    thread_num = re.search(r'thread\/[\w]{1,9}', thread).group(0)
    thread_num = thread_num.replace('thread/', '')

    save_path = f'{os.path.expanduser("~")}/Downloads/4ch/{board}/{thread_num}'
    os.makedirs(save_path, exist_ok=True)
    images = get_images_list(soup, board, thread_num)
    total_images = len(images)

    for image, _ in zip(images, tqdm(range(total_images))):
        save(save_path, image)
